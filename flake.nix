{
  description = "Get PKG links using account sso";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-20.09";

  outputs = { self, nixpkgs }: {

    defaultPackage.x86_64-linux = let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in pkgs.buildGoPackage {
      name = "get-pkg-account";
      src = ./.;
      goPackagePath = "gitlab.com/nopaystation_repos/get-pkg-account";
      #buildFlagsArray = ["-ldflags=-I=/lib64/ld-linux-x86-64.so.2"];
    };

    devShell.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.mkShell {
      NIX_PATH = "nixpkgs=${nixpkgs}";

      buildInputs = with nixpkgs.legacyPackages.x86_64-linux; [ 
        go
        gopls
      ];
    };
  };
}
