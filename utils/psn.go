package utils

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"regexp"
	"strconv"
)

type PSNEntitlementsResponse struct {
	Entitlements []PSNEntitlement `json:"entitlements"`
	MetaRevision int              `json:"meta_revision"`
	RevisionId   int              `json:"revision_id"`
	Start        int              `json:"start"`
	TotalResults int              `json:"total_results"`
}

type PSNEntitlement struct {
	Id                    string `json:"id"`
	ActiveDate            string `json:"active_date"`
	EntitlementAttributes []*struct {
		PlatformId string `json:"platform_id"`
	} `json:"entitlement_attributes"`
	GameMeta struct {
		Name string `json:"name"`
	} `json:"game_meta"`
	DRMDef *struct {
		ContentType string `json:"contentType"`
		ContentName string `json:"contentName"`
		ProductId   string `json:"productId"`
		DRMContents []*struct {
			ContentUrl  string `json:"contentUrl"`
			ContentSize int    `json:"contentSize"`
			PlatformId  int    `json:"platformIds"`
		} `json:"drmContents"`
	} `json:"drm_def"`
}

type PSNClient struct {
	Client        *fasthttp.Client
	access_token  string
	refresh_token string
	SSOToken      string
}

// Adapted from https://github.com/games-directory/api-psn
func (p *PSNClient) Init() {
	res := fasthttp.AcquireRequest()
	uri := fasthttp.URI{}
	uri.Parse(nil, []byte("https://ca.account.sony.com/api/authz/v3/oauth/authorize"))
	uri.QueryArgs().Add("access_type", "offline")
	uri.QueryArgs().Add("client_id", "09515159-7237-4370-9b40-3806e67c0891")
	uri.QueryArgs().Add("redirect_uri", "com.scee.psxandroid.scecompcall://redirect")
	uri.QueryArgs().Add("response_type", "code")
	uri.QueryArgs().Add("scope", "psn:clientapp psn:mobile.v2.core")
	res.SetRequestURI(string(uri.FullURI()))
	res.Header.SetCookie("npsso", p.SSOToken)

	resp := fasthttp.AcquireResponse()

	p.Client.Do(res, resp)

	location := string(resp.Header.Peek("location"))

	locationRegex, _ := regexp.Compile("(code|cid)=(?P<id>[A-Za-z0-9:\\?_\\-\\.\\/=]+)")

	var code string

	if matches := locationRegex.FindAllStringSubmatch(location, -1); len(matches) != 0 {
		code = matches[0][2]
	}

	res = fasthttp.AcquireRequest()
	uri = fasthttp.URI{}
	uri.Parse(nil, []byte("https://ca.account.sony.com/api/authz/v3/oauth/token"))
	//uri.QueryArgs().Add("access_type", "offline")
	//uri.QueryArgs().Add("client_id", "09515159-7237-4370-9b40-3806e67c0891")
	//uri.QueryArgs().Add("redirect_uri", "com.scee.psxandroid.scecompcall://redirect")
	//uri.QueryArgs().Add("response_type", "code")
	//uri.QueryArgs().Add("scope", "psn:clientapp psn:mobile.v2.core")
	res.SetRequestURI(string(uri.FullURI()))
	res.Header.SetMethod("POST")
	res.PostArgs().Add("code", code)
	res.PostArgs().Add("grant_type", "authorization_code")
	res.PostArgs().Add("redirect_uri", "com.scee.psxandroid.scecompcall://redirect")
	res.PostArgs().Add("scope", "psn:clientapp psn:mobile.v2.core")
	res.PostArgs().Add("token_format", "jwt")
	res.Header.SetHost("ca.account.sony.com")
	res.Header.SetReferer("https://my.playstation.com/")
	res.Header.Set("Authorization", "Basic MDk1MTUxNTktNzIzNy00MzcwLTliNDAtMzgwNmU2N2MwODkxOnVjUGprYTV0bnRCMktxc1A=")

	resp = fasthttp.AcquireResponse()

	p.Client.Do(res, resp)

	jsonResp := make(map[string]string)
	json.Unmarshal(resp.Body(), &jsonResp)

	p.access_token = jsonResp["access_token"]
	p.refresh_token = jsonResp["refresh_token"]
}

func (p *PSNClient) GetEntitlements(start int) PSNEntitlementsResponse {
	res := fasthttp.AcquireRequest()
	uri := fasthttp.URI{}
	uri.Parse(nil, []byte("https://commerce.api.np.km.playstation.net/commerce/api/v1/users/me/internal_entitlements"))
	uri.QueryArgs().Add("size", "10000")
	uri.QueryArgs().Add("start", strconv.Itoa(start))
	uri.QueryArgs().Add("fields", "meta_rev,cloud_meta,reward_meta,game_meta,drm_def,drm_def.content_type")

	res.SetRequestURI(string(uri.FullURI()))
	res.Header.Add("Authorization", fmt.Sprintf("Bearer %s", p.access_token))

	resp := fasthttp.AcquireResponse()

	p.Client.Do(res, resp)

	psnResp := &PSNEntitlementsResponse{}

	json.Unmarshal(resp.Body(), psnResp)

	return *psnResp
}

func (p *PSNClient) GetPSDLEFormat(entitlements PSNEntitlementsResponse) PSDLEResp {
	psdleItems := make([]PSDLEItem, 0)

	for _, v := range entitlements.Entitlements {
		if v.DRMDef != nil {
			platformId := v.DRMDef.DRMContents[0].PlatformId
			matched, _ := regexp.MatchString(".*-PC.*", v.Id)
			platform := PlatformIds[platformId]

			if platform != "" && matched {
				platform = "PSV"
			}

			if platform != "" {
				psdleItems = append(psdleItems, PSDLEItem{
					Id:        v.Id,
					Name:      v.DRMDef.ContentName,
					PKG:       v.DRMDef.DRMContents[0].ContentUrl,
					Platform:  platform,
					ProductId: v.DRMDef.ProductId,
					Size:      v.DRMDef.DRMContents[0].ContentSize,
					URL:       "",
				})
			}
		}
	}

	return PSDLEResp{
		Items: psdleItems,
	}
}

var PlatformIds = map[int]string{
	2147483648: "PS3",
	2281701376: "PSV",
	4161798144: "PSP",
	1880096768: "PSP",
	4027580416: "PSP",
}

type PSDLEResp struct {
	Items []PSDLEItem `json:"items"`
}

type PSDLEItem struct {
	Platform  string `json:"platform"`
	Name      string `json:"name"`
	PKG       string `json:"pkg"`
	Id        string `json:"id"`
	Size      int    `json:"size"`
	ProductId string `json:"productId"`
	URL       string `json:"url"`
}
