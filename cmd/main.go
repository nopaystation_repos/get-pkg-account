package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"unicode/utf8"

	"encoding/csv"
	"encoding/json"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
	"gitlab.com/nopaystation_repos/get-pkg-account/utils"
)

var psnListCommand = &cobra.Command{
	Use:   "list",
	Short: "List all entitlements",
	Run: func(cmd *cobra.Command, args []string) {
		purchasedDate, _ := cmd.PersistentFlags().GetBool("purchase-date")
		allTitles, _ := cmd.PersistentFlags().GetBool("all-titles")
		psnClient := utils.PSNClient{
			Client:   &fasthttp.Client{},
			SSOToken: cmd.PersistentFlags().Lookup("ssotoken").Value.String(),
		}
		psdle, _ := cmd.PersistentFlags().GetBool("psdle")
		psnClient.Init()
		psnResp := psnClient.GetEntitlements(0)

		if psnResp.TotalResults != len(psnResp.Entitlements) {
			counter := 1
			for true {
				tempResp := psnClient.GetEntitlements(counter * 10000)
				psnResp.Entitlements = append(psnResp.Entitlements, tempResp.Entitlements...)
				if psnResp.TotalResults == len(psnResp.Entitlements) {
					break
				}
				counter++
			}
		}

		if psdle {
			psdleOut := psnClient.GetPSDLEFormat(psnResp)
			if split, _ := cmd.PersistentFlags().GetBool("split"); !split {
				out, _ := json.Marshal(psdleOut)
				fmt.Print(string(out))
			} else {
				splitSize, _ := cmd.PersistentFlags().GetInt("split-size")
				outputName, _ := cmd.PersistentFlags().GetString("output-name")

				counter := 0
				for true {
					if len(psdleOut.Items) > splitSize {
						currentItems, _ := json.Marshal(utils.PSDLEResp{
							Items: psdleOut.Items[0:splitSize],
						})

						ioutil.WriteFile(fmt.Sprintf("%s-%d.json", outputName, counter), currentItems, 0644)
						psdleOut.Items = psdleOut.Items[splitSize:len(psdleOut.Items)]
						counter++
					} else {
						currentItems, _ := json.Marshal(psdleOut)
						ioutil.WriteFile(fmt.Sprintf("%s-%d.json", outputName, counter), currentItems, 0644)
						break
					}
				}
			}
		} else {
			w := csv.NewWriter(os.Stdout)
			header := []string{"id", "name", "platform", "pkg_link"}
			if purchasedDate {
				header = []string{"id", "name", "platform", "pkg_link", "purchased_date"}
			}

			w.Write(header)

			for _, v := range psnResp.Entitlements {
				if v.DRMDef != nil {
					platformId := v.DRMDef.DRMContents[0].PlatformId
					if purchasedDate {
						w.Write([]string{v.Id, v.DRMDef.ContentName, utils.PlatformIds[platformId], v.DRMDef.DRMContents[0].ContentUrl, v.ActiveDate})
					} else {
						w.Write([]string{v.Id, v.DRMDef.ContentName, utils.PlatformIds[platformId], v.DRMDef.DRMContents[0].ContentUrl})
					}
				} else if allTitles && v.EntitlementAttributes != nil && len(v.EntitlementAttributes) != 0 {
					if purchasedDate {
						w.Write([]string{v.Id, v.GameMeta.Name, strings.ToUpper(v.EntitlementAttributes[0].PlatformId), "EMPTY", v.ActiveDate})
					} else {
						w.Write([]string{v.Id, v.GameMeta.Name, strings.ToUpper(v.EntitlementAttributes[0].PlatformId), "EMPTY"})
					}
				}
			}

			defer w.Flush()
		}
	},
}

var psnReformatCommand = &cobra.Command{
	Use:   "reformat <psdle json file>",
	Short: "Fix any older version psdle jsons",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		psdleFile, _ := ioutil.ReadFile(args[0])

		if !utf8.Valid(psdleFile) {
			cleanedUTFPsdleFile, _ := utils.DecodeUTF16(psdleFile)
			cleanedUTFPsdleFile = string(bytes.TrimPrefix([]byte(cleanedUTFPsdleFile), []byte("\xef\xbb\xbf")))

			uni := regexp.MustCompile(`\\u\S*`)

			psdleFile = []byte(uni.ReplaceAllString(cleanedUTFPsdleFile, ""))
		}

		psdleFormat := &utils.PSDLEResp{}

		json.Unmarshal(psdleFile, &psdleFormat)

		resultPsdle := utils.PSDLEResp{}

		for _, item := range psdleFormat.Items {
			matched, _ := regexp.MatchString(".*-PC.*", item.Id)

			if matched {
				item.Platform = "PSV"
			}

			if item.Platform != "" {
				if item.Platform == "PSP Demos/Themes" {
					item.Platform = "PSP"
				} else if item.Platform == "PSP Demos" {
					item.Platform = "PSP"
				}

				resultPsdle.Items = append(resultPsdle.Items, item)
			}
		}

		out, _ := json.Marshal(resultPsdle)
		fmt.Print(string(out))
	},
}

var (
	// Used for flags.
	cfgFile string

	rootCmd = &cobra.Command{
		Use:   "get-pkg-account",
		Short: "A CLI Tool for PSN entitlements",
	}
)

func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)

	psnListCommand.PersistentFlags().String("ssotoken", "", "SSO Token from https://ca.account.sony.com/api/v1/ssocookie")
	psnListCommand.PersistentFlags().Bool("psdle", false, "PSDLE output format for NPS, default is csv")
	psnListCommand.PersistentFlags().Bool("split", false, "Split output, use with --psdle")
	psnListCommand.PersistentFlags().Int("split-size", 300, "Split size")
	psnListCommand.PersistentFlags().String("output-name", "psdle", "split file output (<name>-0.json, etc)")
	psnListCommand.PersistentFlags().Bool("purchase-date", false, "add purchased date to the export, default to false, only for csv")
	psnListCommand.PersistentFlags().Bool("all-titles", false, "list titles including ps4 and ps5(there won't be pkg links however), default to false, only for csv")
	rootCmd.AddCommand(psnListCommand)
	rootCmd.AddCommand(psnReformatCommand)
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		viper.AddConfigPath(home)
		viper.SetConfigName(".cobra")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
